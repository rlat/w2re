Change log
==========

0.2 2014-10-12 Radek Lát
----------------------------

* Renamed from PrefixTree to w2re.
* Removed some unused functionality from the underlying PrefixTree structure.
* Added command line options.

0.1 2014-02-02 Radek Lát
----------------------------

* Initial release.